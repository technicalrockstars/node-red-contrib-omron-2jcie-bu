const SerialPort = require('serialport');

let res = new Uint8Array(30);
let reslen = 0;

const req = new Uint8Array(9);
req[0] = 0x52;
req[1] = 0x42;
req[2] = 0x05;
req[3] = 0x00;
req[4] = 0x01;
req[5] = 0x22;
req[6] = 0x50;
req[7] = 0xe2;
req[8] = 0xbb;
console.log(req);

const port = new SerialPort("/dev/ttyUSB0", {
    baudRate: 115200,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false
}, function(err) {
    if (err) {
        return console.log('Error on write: ', err.message);
    }
    console.log('Serial port is opened.');
});

port.on('data', function(data) {
    console.log('Data:', data);

    for(var i = 0; i < data.length; i++) {
        res[reslen + i] = data[i];
    }
    reslen = reslen + data.length;
    console.log('Data length=' + data.length);

    if (reslen >= 30) {
        console.log('Response:', data);
        var temp = (res[8] + res[9] * 256) * 0.01;
        var humidity = (res[10] + res[11] * 256) * 0.01;
        var noise = (res[18] + data[19] * 256) * 0.01;
        var co2 = res[22] + res[23] * 256;

        console.log(`Temperature : ${temp} °C`);
        console.log(`Humidity : ${humidity} %`);
        console.log(`Sound Noise : ${noise} dB`);
        console.log(`eCO2 : ${co2} ppm`);
    } 
});

function request() {
    port.write(req, function(err) {
        if (err) {
            return console.log('Error on write: ', err.message);
        }
        console.log('message written');
        reslen = 0;
        for(var i = 0; i < 30; i++) {
            res[i] = 0;
        }
    });
}

setInterval(request, 10000);

