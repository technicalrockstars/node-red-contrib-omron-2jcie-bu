const SerialPort = require('serialport');

const ERR_CODE_CRC = 0x01;
const ERR_CODE_COMMAND = 0x02;
const ERR_CODE_ADDRESS = 0x03;
const ERR_CODE_LENGTH = 0x04;
const ERR_CODE_DATA = 0x05;
const ERR_CODE_BUSY = 0x06;

const USB_CMD_READ = 0x01;
const USB_CMD_WRITE = 0x02;
const USB_CMD_ERR_READ = 0x81;
const USB_CMD_ERR_WRITE = 0x82;
const USB_CMD_UNKNOWN = 0xFF;

const USB_ADDR_LATEST_DATA_LONG = 0x5021;

const USB_EVENT_TARGET_TEMP = 0;
const USB_EVENT_TARGET_HUMIDITY = 1;
const USB_EVENT_TARGET_LIGHT = 2;
const USB_EVENT_TARGET_PRESSURE = 3;
const USB_EVENT_TARGET_NOISE = 4;
const USB_EVENT_TARGET_ETVOC = 5;
const USB_EVENT_TARGET_ECO2 = 6;
const USB_EVENT_TARGET_DISCOMFORT = 7;
const USB_EVENT_TARGET_HEAT = 8;

const USB_EVENT_TYPE_SIMPLE_UPPER = 0x0001;
const USB_EVENT_TYPE_SIMPLE_LOWER = 0x0004;
const USB_EVENT_TYPE_SIMPLE_BOTH = 0x0005;
const USB_EVENT_TYPE_CHANGE_UPPER = 0x0010;
const USB_EVENT_TYPE_CHANGE_LOWER = 0x0040;
const USB_EVENT_TYPE_CHANGE_BOTH = 0x0050;
const USB_EVENT_TYPE_AVERAGE_UPPER = 0x0100;
const USB_EVENT_TYPE_AVERAGE_LOWER = 0x0200;
const USB_EVENT_TYPE_AVERAGE_BOTH = 0x0300;
const USB_EVENT_TYPE_PEEK_UPPER = 0x0400;
const USB_EVENT_TYPE_PEEK_LOWER = 0x0800;
const USB_EVENT_TYPE_PEEK_BOTH = 0x0C00;
const USB_EVENT_TYPE_INTERVAL_DIFF_UPPER = 0x1000;
const USB_EVENT_TYPE_INTERVAL_DIFF_LOWER = 0x2000;
const USB_EVENT_TYPE_INTERVAL_DIFF_BOTH = 0x3000;
const USB_EVENT_TYPE_BASE_DIFF_UPPER = 0x4000;
const USB_EVENT_TYPE_BASE_DIFF_LOWER = 0x8000;
const USB_EVENT_TYPE_BASE_DIFF_BOTH = 0xC000;

const USB_ADDR_LED_SETTING_NORMAL = 0x5111;
const USB_ADDR_LED_SETTING_EVENT = 0x5112;
const USB_ADDR_EVENT_TEMP1 = 0x5211;
const USB_ADDR_EVENT_TEMP2 = 0x5212;
const USB_ADDR_EVENT_HUMIDITY1 = 0x5213;
const USB_ADDR_EVENT_HUMIDITY2 = 0x5214;
const USB_ADDR_EVENT_LIGHT1 = 0x5215;
const USB_ADDR_EVENT_LIGHT2 = 0x5216;
const USB_ADDR_EVENT_PRESSURE1 = 0x5217;
const USB_ADDR_EVENT_PRESSURE2 = 0x5218;
const USB_ADDR_EVENT_NOISE1 = 0x5219;
const USB_ADDR_EVENT_NOISE2 = 0x521A;
const USB_ADDR_EVENT_ETVOC1 = 0x521B;
const USB_ADDR_EVENT_ETVOC2 = 0x521C;
const USB_ADDR_EVENT_ECO21 = 0x521D;
const USB_ADDR_EVENT_ECO22 = 0x521E;
const USB_ADDR_EVENT_DISCOMFORT1 = 0x521F;
const USB_ADDR_EVENT_DISCOMFORT2 = 0x5220;
const USB_ADDR_EVENT_HEATSTROKE1 = 0x5221;
const USB_ADDR_EVENT_HEATSTROKE2 = 0x5222;

// ノード全体の状態
const NODE_STATUS_INITIALIZING = Symbol('初期化中');
const NODE_STATUS_SENSING = Symbol('センサーデータ収集中（初期化処理完了）');
const NODE_STATUS_FINALIZING = Symbol('終了中');
const NODE_STATUS_FAILED_TO_OPEN_PORT = Symbol('ポートオープン失敗');

// 初期化処理のサブ状態
const INIT_SUB_STATUS_BEFORE_SETTING = Symbol('未設定');
const INIT_SUB_STATUS_TEMPERATURE_SENSOR1_GETTING = Symbol('温度センサー1の設定取得中');
const INIT_SUB_STATUS_TEMPERATURE_SENSOR1_SETTING = Symbol('温度センサー1の設定中');
const INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING = Symbol('温度センサー2の設定取得中');
const INIT_SUB_STATUS_TEMPERATURE_SENSOR2_SETTING = Symbol('温度センサー2の設定中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING = Symbol('湿度センサー1の設定取得中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR1_SETTING = Symbol('湿度センサー1の設定中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING = Symbol('湿度センサー2の設定取得中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR2_SETTING = Symbol('湿度センサー2の設定中');
const INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING = Symbol('照度センサー1の設定取得中');
const INIT_SUB_STATUS_LIGHT_SENSOR1_SETTING = Symbol('照度センサー1の設定中');
const INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING = Symbol('照度センサー2の設定取得中');
const INIT_SUB_STATUS_LIGHT_SENSOR2_SETTING = Symbol('照度センサー2の設定中');
const INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING = Symbol('気圧センサー1の設定取得中');
const INIT_SUB_STATUS_PRESSURE_SENSOR1_SETTING = Symbol('気圧センサー1の設定中');
const INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING = Symbol('気圧センサー2の設定取得中');
const INIT_SUB_STATUS_PRESSURE_SENSOR2_SETTING = Symbol('気圧センサー2の設定中');
const INIT_SUB_STATUS_NOISE_SENSOR1_GETTING = Symbol('騒音センサー1の設定取得中');
const INIT_SUB_STATUS_NOISE_SENSOR1_SETTING = Symbol('騒音センサー1の設定中');
const INIT_SUB_STATUS_NOISE_SENSOR2_GETTING = Symbol('騒音センサー2の設定取得中');
const INIT_SUB_STATUS_NOISE_SENSOR2_SETTING = Symbol('騒音センサー2の設定中');
const INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING = Symbol('eTVOCセンサー1の設定取得中');
const INIT_SUB_STATUS_ETVOC_SENSOR1_SETTING = Symbol('eTVOCセンサー1の設定中');
const INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING = Symbol('eTVOCセンサー2の設定取得中');
const INIT_SUB_STATUS_ETVOC_SENSOR2_SETTING = Symbol('eTVOCセンサー2の設定中');
const INIT_SUB_STATUS_ECO2_SENSOR1_GETTING = Symbol('eCO2センサー1の設定取得中');
const INIT_SUB_STATUS_ECO2_SENSOR1_SETTING = Symbol('eCO2センサー1の設定中');
const INIT_SUB_STATUS_ECO2_SENSOR2_GETTING = Symbol('eCO2センサー2の設定取得中');
const INIT_SUB_STATUS_ECO2_SENSOR2_SETTING = Symbol('eCO2センサー2の設定中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING = Symbol('不快指数センサー1の設定取得中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR1_SETTING = Symbol('不快指数センサー1の設定中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING = Symbol('不快指数センサー2の設定取得中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR2_SETTING = Symbol('不快指数センサー2の設定中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING = Symbol('熱中症危険度センサー1の設定取得中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR1_SETTING = Symbol('熱中症危険度センサー1の設定中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING = Symbol('熱中症危険度センサー2の設定取得中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR2_SETTING = Symbol('熱中症危険度センサー2の設定中');
const INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING = Symbol('LED setting [event state]の取得中');
const INIT_SUB_STATUS_LED_SETTING_EVENT_SETTING = Symbol('LED setting [event state]の設定中');
const INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING = Symbol('LED setting [normal state]の取得中');
const INIT_SUB_STATUS_LED_SETTING_NORMAL_SETTING = Symbol('LED setting [normal state]の設定中');
const INIT_SUB_STATUS_SETTING_DONE = Symbol('イベント設定完了');

const INIT_SUB_STATUS_TEMPERATURE_SENSOR1_SETTING_REQ = Symbol('温度センサー1の設定リクエスト中');
const INIT_SUB_STATUS_TEMPERATURE_SENSOR2_SETTING_REQ = Symbol('温度センサー2の設定リクエスト中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR1_SETTING_REQ = Symbol('湿度センサー1の設定リクエスト中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR2_SETTING_REQ = Symbol('湿度センサー2の設定リクエスト中');
const INIT_SUB_STATUS_LIGHT_SENSOR1_SETTING_REQ = Symbol('照度センサー1の設定リクエスト中');
const INIT_SUB_STATUS_LIGHT_SENSOR2_SETTING_REQ = Symbol('照度センサー2の設定リクエスト中');
const INIT_SUB_STATUS_PRESSURE_SENSOR1_SETTING_REQ = Symbol('気圧センサー1の設定リクエスト中');
const INIT_SUB_STATUS_PRESSURE_SENSOR2_SETTING_REQ = Symbol('気圧センサー2の設定リクエスト中');
const INIT_SUB_STATUS_NOISE_SENSOR1_SETTING_REQ = Symbol('騒音センサー1の設定リクエスト中');
const INIT_SUB_STATUS_NOISE_SENSOR2_SETTING_REQ = Symbol('騒音センサー2の設定リクエスト中');
const INIT_SUB_STATUS_ETVOC_SENSOR1_SETTING_REQ = Symbol('eTVOCセンサー1の設定リクエスト中');
const INIT_SUB_STATUS_ETVOC_SENSOR2_SETTING_REQ = Symbol('eTVOCセンサー2の設定リクエスト中');
const INIT_SUB_STATUS_ECO2_SENSOR1_SETTING_REQ = Symbol('eCO2センサー1の設定リクエスト中');
const INIT_SUB_STATUS_ECO2_SENSOR2_SETTING_REQ = Symbol('eCO2センサー2の設定リクエスト中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR1_SETTING_REQ = Symbol('不快指数センサー1の設定リクエスト中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR2_SETTING_REQ = Symbol('不快指数センサー2の設定リクエスト中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR1_SETTING_REQ = Symbol('熱中症危険度センサー1の設定リクエスト中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR2_SETTING_REQ = Symbol('熱中症危険度センサー2の設定リクエスト中');
const INIT_SUB_STATUS_LED_SETTING_EVENT_SETTING_REQ = Symbol('LED setting [event state]の設定リクエスト中');
const INIT_SUB_STATUS_LED_SETTING_NORMAL_SETTING_REQ = Symbol('LED setting [normal state]の設定リクエスト中');

const INIT_SUB_STATUS_TEMPERATURE_SENSOR1_GETTING_REQ = Symbol('温度センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING_REQ = Symbol('温度センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING_REQ = Symbol('湿度センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING_REQ = Symbol('湿度センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING_REQ = Symbol('照度センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING_REQ = Symbol('照度センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING_REQ = Symbol('気圧センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING_REQ = Symbol('気圧センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_NOISE_SENSOR1_GETTING_REQ = Symbol('騒音センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_NOISE_SENSOR2_GETTING_REQ = Symbol('騒音センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING_REQ = Symbol('eTVOCセンサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING_REQ = Symbol('eTVOCセンサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_ECO2_SENSOR1_GETTING_REQ = Symbol('eCO2センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_ECO2_SENSOR2_GETTING_REQ = Symbol('eCO2センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING_REQ = Symbol('不快指数センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING_REQ = Symbol('不快指数センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING_REQ = Symbol('熱中症危険度センサー1の設定取得リクエスト中');
const INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING_REQ = Symbol('熱中症危険度センサー2の設定取得リクエスト中');
const INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING_REQ = Symbol('LED setting [event state]の設定取得リクエスト中');
const INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING_REQ = Symbol('LED setting [normal state]の設定取得リクエスト中');

const SENSOR_INITIALIZE_TIMEOUT = 5000;       // センサーの初期化のタイムアウト時間(ms)

module.exports = function(RED) {

    function Omron2jcieBu(config) {

        RED.nodes.createNode(this,config);
        // config data の設定
        this.port = config.port;
        // event1の設定
        this.event_target = config.event_target;
        this.event_type = config.event_type;
        this.event_upper = config.event_upper;
        this.event_lower = config.event_lower;
        this.event_count = config.event_count;
        // event2の設定
        this.event2_target = config.event2_target;
        this.event2_type = config.event2_type;
        this.event2_upper = config.event2_upper;
        this.event2_lower = config.event2_lower;
        this.event2_count = config.event2_count;
        // event3の設定
        this.event3_target = config.event3_target;
        this.event3_type = config.event3_type;
        this.event3_upper = config.event3_upper;
        this.event3_lower = config.event3_lower;
        this.event3_count = config.event3_count;
        // LED eventの設定
        this.led_event_rule = config.led_event_rule;
        this.led_event_red = config.led_event_red;
        this.led_event_green = config.led_event_green;
        this.led_event_blue = config.led_event_blue;
        // LED nomalの設定
        this.led_normal_rule = config.led_normal_rule;
        this.led_normal_red = config.led_normal_red;
        this.led_normal_green = config.led_normal_green;
        this.led_normal_blue = config.led_normal_blue;

        // ステータス設定
        this.nodeStatus = {
            mainStatus: NODE_STATUS_INITIALIZING,
            initSubStatus: INIT_SUB_STATUS_BEFORE_SETTING
        }

        let node = this;

        // Serial Portからのレスポンスデータ受信用バッファ
        let res = new Uint8Array(60);
        let reslen = 0;

        // Latest Data Long 読み込みコマンドの設定
        const req = setFrame(setPayload(USB_CMD_READ, USB_ADDR_LATEST_DATA_LONG, null), 3);
        node.log("req:" + req.toString(16));

        const port = new SerialPort(node.port, {
            baudRate: 115200,
            dataBits: 8,
            parity: 'none',
            stopBits: 1,
            flowControl: false
        }, function(err) {
            if (err) {
                sendErrorMessage(node, `Failed to open serial port: ${err.message}`, `Failed to open serial port: ${err.message}`);
                node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
                node.error('Error on write: ', err.message);
            }
            //ポートのオープンに成功した場合
            node.log('Serial port is opened.');

            //センサー初期化のタイムアウトを設定
            setTimeout(() => {
                if(node.nodeStatus.mainStatus === NODE_STATUS_INITIALIZING) {
                    node.nodeStatus.mainStatus = NODE_STATUS_SENSING;
                    reslen = 0;
                    res.fill(0); 
                    node.log('センサーの初期化にタイムアウトしました');
                }
            }, SENSOR_INITIALIZE_TIMEOUT)
            
            // センサーの初期化処理を開始
            initialize(node, port, null);
        });

        /**
         * シリアルポートのデータ受信処理
         */
        port.on('data', function(data) {
            node.log("serial data receive");
            res.set(data, reslen);
            reslen += data.length;
            if (reslen >= (getFrameLength(res) + 4)) {
                const cmd = getCommand(res);
                const addr = getAddress(res);
                node.log("cmd=" + cmd.toString(16) + " / address=" + addr.toString(16));

                // 設定エラー出力
                switch(cmd) {
                    case USB_CMD_READ:
                        break;
                    case USB_CMD_WRITE:
                        break;
                    case USB_CMD_ERR_READ:
                    case USB_CMD_ERR_WRITE:
                    case USB_CMD_UNKNOWN:
                    default:
                        node.error('error response');
                        let send = function() { node.send.apply(node,arguments) };
                        let msg = {};
                        msg.payload = {
                            'errorCode' : res[7].toString(16),
                            'errorMessage' : getErrorMsg(res[7])
                        };
                        send(msg);
                }

                // ステータスごとの処理
                switch(node.nodeStatus.mainStatus) {
                    case NODE_STATUS_SENSING:
                        if (addr == USB_ADDR_LATEST_DATA_LONG) {
                            node.log("USB_CMD_READ : USB_ADDR_LATEST_DATA_LONG");
                            sendSensorData(node, res);
                        } else {
                            node.error("RESPONSE ERROR : expected LATEST DATA LONG FORMAT. received: " + addr.toString(16));
                        }
                        break;
                    case NODE_STATUS_INITIALIZING:
                        while(true) {
                            initialize(node, port, res);
                            if (node.nodeStatus.mainStatus !== NODE_STATUS_INITIALIZING) {
                                break;
                            }
                            if ((node.nodeStatus.initSubStatus === INIT_SUB_STATUS_TEMPERATURE_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_TEMPERATURE_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HUMIDITY_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HUMIDITY_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LIGHT_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LIGHT_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_PRESSURE_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_PRESSURE_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_NOISE_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_NOISE_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ETVOC_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ETVOC_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ECO2_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ECO2_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_DISCOMFORT_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_DISCOMFORT_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HEATSTROKE_SENSOR1_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HEATSTROKE_SENSOR2_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LED_SETTING_EVENT_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LED_SETTING_NORMAL_SETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_TEMPERATURE_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_NOISE_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_NOISE_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ECO2_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_ECO2_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING_REQ) ||
                                (node.nodeStatus.initSubStatus === INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING_REQ)) {
                              break;
                            } else if(node.nodeStatus.initSubStatus === INIT_SUB_STATUS_SETTING_DONE) {
                                node.log('finished initializing');
                                node.nodeStatus.mainStatus = NODE_STATUS_SENSING;
                                break;
                            }

                        }
                        break;
                    case NODE_STATUS_FINALIZING:
                        break;
                    case NODE_STATUS_FAILED_TO_OPEN_PORT:
                        break;
                    default:
                        node.log('UNKNOWN NODE STATUS');
                }
                reslen = 0;
                res.fill(0);    
            } 
        });

        /**
         * シリアル通信エラー（例外）処理
         */
        port.on('error', function(err) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
            sendErrorMessage(node, `serial port error: ${err}`, `serial port error: ${err}`);
            node.error(`serial port error: ${err}`);
        });

        node.on('input', function(msg,send,done) {
            // ステータスごとの処理
            switch(node.nodeStatus.mainStatus) {
                case NODE_STATUS_SENSING:
                    if (!writePort(node, port, req)) {
                        node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
                    };
                    reslen = 0;
                    res.fill(0);
                    break;
                case NODE_STATUS_INITIALIZING:
                case NODE_STATUS_FINALIZING:
                    //NOP
                    break;
                case NODE_STATUS_FAILED_TO_OPEN_PORT:
                    sendErrorMessage(node, `serial port error`, `serial port error`);
                    node.error('serial port error');
                    break;
                default:
                    node.log('UNKNOWN NODE STATUS');
            }
            if(done) {
                done();
            }
        });

        node.on('close', function() {
            node.nodeStatus.mainStatus = NODE_STATUS_FINALIZING;
            if (port.isOpen) {
                port.close(function(err) {
                    if (err) {
                        sendErrorMessage(node, `Failed to close port: ${err.message}`, `Failed to close port: ${err.message}`);
                        node.error('Error on write: ', err.message);
                    }
                });
                node.log('Port Closed');
            }
        });
    }

    /*
        USB Communication の Common frame formatを設定
        payload : payloadデータのバッファ
        plength : payloadデータの長さ
    */
    function setFrame(payload, plength) {
        let frame = new Uint8Array(plength + 6);
        frame[0] = 0x52;
        frame[1] = 0x42;
        frame[2] = (plength + 2) % 256;
        frame[3] = (plength + 2) >> 8;
        frame.set(payload, 4);
        let crc = crc16(frame, plength + 4);
        frame[plength + 4] = crc % 256;
        frame[plength + 5] = crc >> 8;
        return frame;
    }
    
    /*
        CRC16の計算処理
        buff : 計算対象のバッファ
        bufflength : バッファの長さ 
    */
    function crc16(buff, bufflength) {
        let crc = 0xffff;
        for(let i = 0; i < bufflength; i++) {
            crc = crc ^ buff[i];
            for(let j = 0; j < 8; j++) {
                if ((crc & 0x0001) > 0) {
                    crc = crc >> 1;
                    crc = crc ^ 0xA001;
                } else {
                    crc = crc >> 1;
                }
            }
        }
        return crc;
    }

    /*
        USB Communication の Payload frame formatを設定
        cmd : Command(0x01:read, 0x02:write)
        addr : Address
        data : Dataバッファ
    */
    function setPayload(cmd, addr, data) {
        let length = 3;
        if (data) {
            length = length + data.length;
        }
        let payload = new Uint8Array(length);
        payload[0] = cmd;
        payload[1] = addr % 256;
        payload[2] = addr >> 8;
        if (data) {
            payload.set(data, 3);
        }
        return payload;
    }

    /*
        USB Communication の LED Setting frame formatを設定
        addr : Address()
        rule : display rule
        red : Intensity of LED (Red)
        green : Intensity of LED (Green)
        blue : Intensity of LED (Blue) 
    */
    function setLEDSetting(addr, rule, red, green, blue) {
        let led_cmd = new Uint8Array(5);
        led_cmd[0] = rule % 256;
        led_cmd[1] = rule >> 8;
        led_cmd[2] = red;
        led_cmd[3] = green;
        led_cmd[4] = blue;
        return setFrame(setPayload(USB_CMD_WRITE, addr, led_cmd), 8);
    }

    function setEvent1(target, type, upper, lower) {
        let event_cmd = new Uint8Array(20);
        event_cmd.fill(0);

        switch(parseInt(target, 10)) {
            case USB_EVENT_TARGET_TEMP:
                setEventCmd1Temperature(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_TEMP1, event_cmd), 23);
            case USB_EVENT_TARGET_HUMIDITY:
                setEventCmd1Humidity(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_HUMIDITY1, event_cmd), 23);
            case USB_EVENT_TARGET_LIGHT:
                setEventCmd1Light(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_LIGHT1, event_cmd), 23);
            case USB_EVENT_TARGET_PRESSURE:
                setEventCmd1Pressure(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_PRESSURE1, event_cmd), 23);
            case USB_EVENT_TARGET_NOISE:
                setEventCmd1Noise(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_NOISE1, event_cmd), 23);
            case USB_EVENT_TARGET_ETVOC:
                setEventCmd1eTVOC(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_ETVOC1, event_cmd), 23);
            case USB_EVENT_TARGET_ECO2:
                setEventCmd1eCO2(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_ECO21, event_cmd), 23);
            case USB_EVENT_TARGET_DISCOMFORT:
                setEventCmd1Discomfort(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_DISCOMFORT1, event_cmd), 23);
            case USB_EVENT_TARGET_HEAT:
                setEventCmd1Heatstroke(parseInt(type, 16), event_cmd, upper, lower);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_HEATSTROKE1, event_cmd), 23);
            default:
                return null;
        } 
    }

    function setEvent2(target, type, upper, lower, count) {
        let event_cmd = new Uint8Array(20);
        event_cmd.fill(0);

        switch(parseInt(target, 10)) {
            case USB_EVENT_TARGET_TEMP:
                setEventCmd2Temperature(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_TEMP2, event_cmd), 23);
            case USB_EVENT_TARGET_HUMIDITY:
                setEventCmd2Humidity(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_HUMIDITY2, event_cmd), 23);
            case USB_EVENT_TARGET_LIGHT:
                setEventCmd2Light(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_LIGHT2, event_cmd), 23);
            case USB_EVENT_TARGET_PRESSURE:
                setEventCmd2Pressure(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_PRESSURE2, event_cmd), 23);
            case USB_EVENT_TARGET_NOISE:
                setEventCmd2Noise(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_NOISE2, event_cmd), 23);
            case USB_EVENT_TARGET_ETVOC:
                setEventCmd2eTVOC(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_ETVOC2, event_cmd), 23);
            case USB_EVENT_TARGET_ECO2:
                setEventCmd2eCO2(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_ECO22, event_cmd), 23);
            case USB_EVENT_TARGET_DISCOMFORT:
                setEventCmd2Discomfort(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_DISCOMFORT2, event_cmd), 23);
            case USB_EVENT_TARGET_HEAT:
                setEventCmd2Heatstroke(parseInt(type, 16), event_cmd, upper, lower, count);
                return setFrame(setPayload(USB_CMD_WRITE, USB_ADDR_EVENT_HEATSTROKE2, event_cmd), 23);
            default:
                return null;
            } 
    }

    /**
     * 温度向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1Temperature(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
    }

    /**
     * 温度向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2Temperature(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }

    /**
     * 湿度向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1Humidity(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
    }

    /**
     * 湿度向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2Humidity(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }

    /**
     * 明度向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1Light(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
    }

    /**
     * 明度向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2Light(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }

    /**
     * 気圧向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1Pressure(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = 3000 % 256;
                buff[7] = 3000 >> 8;
                buff[8] = 3000 % 256;
                buff[9] = 3000 >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[4] = 3000 % 256;
                buff[5] = 3000 >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                buff[8] = lower % 256;
                buff[9] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                buff[8] = lower % 256;
                buff[9] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[4] = 3000 % 256;
                buff[5] = 3000 >> 8;
                buff[6] = 3000 % 256;
                buff[7] = 3000 >> 8;
                buff[8] = 3000 % 256;
                buff[9] = 3000 >> 8;
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[4] = 3000 % 256;
                buff[5] = 3000 >> 8;
                buff[6] = 3000 % 256;
                buff[7] = 3000 >> 8;
                buff[8] = 3000 % 256;
                buff[9] = 3000 >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[4] = 3000 % 256;
                buff[5] = 3000 >> 8;
                buff[6] = 3000 % 256;
                buff[7] = 3000 >> 8;
                buff[8] = 3000 % 256;
                buff[9] = 3000 >> 8;
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[4] = 3000 % 256;
                buff[5] = 3000 >> 8;
                buff[6] = 3000 % 256;
                buff[7] = 3000 >> 8;
                buff[8] = 3000 % 256;
                buff[9] = 3000 >> 8;
                break;
        }
    }

    /**
     * 気圧向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2Pressure(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                buff[0] = 3000 % 256;
                buff[1] = 3000 >> 8;
                buff[2] = 3000 % 256;
                buff[3] = 3000 >> 8;
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }

    /**
     * 騒音向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1Noise(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = 3300 % 256;
                buff[7] = 3300 >> 8;
                buff[8] = 3300 % 256;
                buff[9] = 3300 >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[4] = 3300 % 256;
                buff[5] = 3300 >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                buff[8] = lower % 256;
                buff[9] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                buff[8] = lower % 256;
                buff[9] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[4] = 3300 % 256;
                buff[5] = 3300 >> 8;
                buff[6] = 3300 % 256;
                buff[7] = 3300 >> 8;
                buff[8] = 3300 % 256;
                buff[9] = 3300 >> 8;
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[2] = 33 % 256;
                buff[3] = 33 >> 8;
                buff[4] = 33 % 256;
                buff[5] = 33 >> 8;
                buff[6] = 33 % 256;
                buff[7] = 33 >> 8;
                buff[8] = 33 % 256;
                buff[9] = 33 >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[4] = 3300 % 256;
                buff[5] = 3300 >> 8;
                buff[6] = 3300 % 256;
                buff[7] = 3300 >> 8;
                buff[8] = 3300 % 256;
                buff[9] = 3300 >> 8;
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[4] = 3300 % 256;
                buff[5] = 3300 >> 8;
                buff[6] = 3300 % 256;
                buff[7] = 3300 >> 8;
                buff[8] = 3300 % 256;
                buff[9] = 3300 >> 8;
                break;
        }
    }

    /**
     * 騒音向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2Noise(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                buff[0] = 3300 % 256;
                buff[1] = 3300 >> 8;
                buff[2] = 3300 % 256;
                buff[3] = 3300 >> 8;
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }

    /**
     * eTVOC向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1eTVOC(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
    }

    /**
     * eTVOC向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2eTVOC(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }
 
    /**
     * eCO2向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1eCO2(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = 400 % 256;
                buff[7] = 400 >> 8;
                buff[8] = 400 % 256;
                buff[9] = 400 >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[4] = 400 % 256;
                buff[5] = 400 >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                buff[8] = lower % 256;
                buff[9] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                buff[8] = lower % 256;
                buff[9] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[4] = 400 % 256;
                buff[5] = 400 >> 8;
                buff[6] = 400 % 256;
                buff[7] = 400 >> 8;
                buff[8] = 400 % 256;
                buff[9] = 400 >> 8;
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[4] = 400 % 256;
                buff[5] = 400 >> 8;
                buff[6] = 400 % 256;
                buff[7] = 400 >> 8;
                buff[8] = 400 % 256;
                buff[9] = 400 >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[4] = 400 % 256;
                buff[5] = 400 >> 8;
                buff[6] = 400 % 256;
                buff[7] = 400 >> 8;
                buff[8] = 400 % 256;
                buff[9] = 400 >> 8;
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[4] = 400 % 256;
                buff[5] = 400 >> 8;
                buff[6] = 400 % 256;
                buff[7] = 400 >> 8;
                buff[8] = 400 % 256;
                buff[9] = 400 >> 8;
                break;
        }
    }

    /**
     * eCO2向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2eCO2(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                buff[0] = 400 % 256;
                buff[1] = 400 >> 8;
                buff[2] = 400 % 256;
                buff[3] = 400 >> 8;
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }
   

    /**
     * 不快指数向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1Discomfort(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
    }

    /**
     * 不快指数向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2Discomfort(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }
 
    /**
     * ヒートストローク向けイベント設定用フレームの作成処理1（simple, change)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     */
    function setEventCmd1Heatstroke(type, buff, upper, lower) {
        buff.fill(0);

        buff[0] = type % 256;
        buff[1] = type >> 8;
        buff[18] = 0xff;
        buff[19] = 0xff;
        switch(type) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                buff[2] = upper % 256;
                buff[3] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                buff[10] = upper % 256;
                buff[11] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
            }
    }

    /**
     * ヒートストローク向けイベント設定用フレームの作成処理2（Average, Peek-to-peek, interval diff, base diff)
     * 
     * @param {*} type 
     * @param {*} buff 
     * @param {*} upper 
     * @param {*} lower 
     * @param {*} count 
     */
    function setEventCmd2Heatstroke(type, buff, upper, lower, count) {
        buff.fill(0);

        switch(type) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                buff[0] = upper % 256;
                buff[1] = upper >> 8;
                buff[2] = lower % 256;
                buff[3] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                buff[4] = upper % 256;
                buff[5] = upper >> 8;
                buff[6] = lower % 256;
                buff[7] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                buff[8] = upper % 256;
                buff[9] = upper >> 8;
                buff[10] = lower % 256;
                buff[11] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                buff[12] = upper % 256;
                buff[13] = upper >> 8;
                buff[14] = lower % 256;
                buff[15] = lower >> 8;
                break;
            default:
                break;
        }
        if ((count >= 0x01) && (count <= 0x08)) {
            buff[16] = count;
            buff[17] = count;
            buff[18] = count;
            buff[19] = count;
        } else {
            buff[16] = 0x08;
            buff[17] = 0x08;
            buff[18] = 0x08;
            buff[19] = 0x08;
        }
    }
   
    /**
     * フレームのValidationチェック処理
     * @param {*} frame 
     */
    function isFrameOK(frame) {
        let header = 0;
        header = frame[0];
        header = header + (frame[1] << 8);
        /** CRCもチェックするべき */
        if (header === 0x4252) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 応答メッセージからのフレーム長取得処理
     * 
     * @param {*} frame 
     */
    function getFrameLength(frame) {
        let len = 0;
        len = frame[2];
        len = len + (frame[3] << 8);
        return len;
    }

    /**
     * 応答メッセージからのコマンド取得処理
     * 
     * @param {*} frame 
     */
    function getCommand(frame) {
        return frame[4];
    }

    /**
     * 応答メッセージからのアドレス取得処理
     * 
     * @param {*} frame 
     */
    function getAddress(frame) {
        let addr = 0;
        addr = frame[5];
        addr = addr + (frame[6] << 8);
        return addr;
    }

    /**
     * ノード出力(msg)の作成処理
     * 
     * @param {*} node 
     * @param {*} res 
     */
    function sendSensorData(node, res) {
        let temp = (res[8] + res[9] * 256) / 100;
        let humidity = (res[10] + res[11] * 256) / 100;
        let light = (res[12] + res[13] * 256);
        let pressure = (res[14] + res[15] * 256 + res[16] * (256 ** 2)  + res[17] * (256 ** 3)) / 1000;
        let noise = (res[18] + res[19] * 256) / 100;
        let tvoc = (res[20] + res[21] * 256);
        let co2 = (res[22] + res[23] * 256);
        let discomfort = (res[24] + res[25] * 256) / 100;
        let heatstroke = (res[26] + res[27] * 256) / 100;
        let evt_temp = (res[35] + res[36] * 256);
        let evt_humidity = (res[37] + res[38] * 256);
        let evt_light = (res[39] + res[40] * 256);
        let evt_pressure = (res[41] + res[42] * 256);
        let evt_noise = (res[43] + res[44] * 256);
        let evt_tvoc = (res[45] + res[46] * 256);
        let evt_co2 = (res[47] + res[48] * 256);
        let evt_discomfort = (res[49] + res[50] * 256);
        let evt_heatstroke = (res[51] + res[52] * 256);

        let msg1 = {};
        msg1.payload = {
            'temperature': temp,
            'relativeHumidity': humidity,
            'ambientLight': light,
            'barometricPressure': pressure,
            'soundNoise': noise,
            'etvoc': tvoc,
            'eco2': co2,
            'discomfortIndex': discomfort,
            'heatStroke': heatstroke
        };

        let msg2 = {};
        msg2.payload = {
            'temperatureEvent': evt_temp,
            'relativeHumidityEvent': evt_humidity,
            'ambientLightEvent': evt_light,
            'barometricPressureEvent': evt_pressure,
            'soundNoiseEvent': evt_noise,
            'etvocEvent': evt_tvoc,
            'eco2Event': evt_co2,
            'discomfortIndexEvent': evt_discomfort,
            'heatStrokeEvent': evt_heatstroke
        };

        let send = function() { node.send.apply(node,arguments) };
        send([msg1, msg2]);
    }

    /**
     * エラーメッセージの送信処理
     * @param {*} node 
     * @param {string} message1
     * @param {string} message2
     */
    let sendErrorMessage = (node,message1,message2) => {
        let send = function() { node.send.apply(node,arguments) };

        let msg1 = null;
        let msg2 = null;

        if (message1) {
            msg1 = { payload: {'errorMessage': message1}};
        }

        if (message2) {
            msg2 = { payload: {'errorMessage': message2}};
        }

        send([msg1,msg2]);                
    }

    /**
     * エラーメッセージの取得処理
     * @param {number} errorCode
     */
    let getErrorMsg = errorCode => {
        let errorMsg = '';
        switch (errorCode) {
            case ERR_CODE_CRC:
                errorMsg = 'CRC error';    
                break;
            case ERR_CODE_COMMAND:
                errorMsg = 'Command error';
                break;
            case ERR_CODE_ADDRESS:
                errorMsg = 'Address error';
                break;
            case ERR_CODE_LENGTH:
                errorMsg = 'Length error';
                break;
            case ERR_CODE_DATA:
                errorMsg = 'Data error';
                break;
            case ERR_CODE_BUSY:
                errorMsg = 'Busy';
                break;
            default:
                errorMsg = null;
        }
        return errorMsg;
    }    

    /**
     * センサー初期化処理
     * 
     * @param {*} node 
     * @param {*} port 
     * @param {*} res
     */
    let initialize = (node, port, res) => {
        switch (node.nodeStatus.initSubStatus) {
            case INIT_SUB_STATUS_BEFORE_SETTING:
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR1_GETTING;
            
            // センサー設定取得のサブステータス
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_TEMP);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_TEMP);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_HUMIDITY);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_HUMIDITY);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_LIGHT);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_LIGHT);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_PRESSURE);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_PRESSURE);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_NOISE);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_NOISE);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR2_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_ETVOC);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_ETVOC);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING_REQ;
                break;    
            case INIT_SUB_STATUS_ECO2_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_ECO2);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_ECO2_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_ECO2);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR2_GETTING_REQ;
                break;    
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_DISCOMFORT);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_DISCOMFORT);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING_REQ;
                break;    
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING:
                readEvent1(node, port, USB_EVENT_TARGET_HEAT);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING:
                readEvent2(node, port, USB_EVENT_TARGET_HEAT);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING:
                readLEDSettingEvent(node, port);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING_REQ;
                break;
            case INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING:
                readLEDSettingNormal(node, port);
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING_REQ;
                break;

            // センサー設定のサブステータス
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR1_SETTING:
                node.log('温度センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_TEMP)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR2_SETTING:
                node.log('温度センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_TEMP)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR1_SETTING:
                node.log('湿度センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_HUMIDITY)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR2_SETTING:
                node.log('湿度センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_HUMIDITY)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR1_SETTING:
                node.log('照度センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_LIGHT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR2_SETTING:
                node.log('照度センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_LIGHT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR1_SETTING:
                node.log('気圧センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_PRESSURE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR2_SETTING:
                node.log('気圧センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_PRESSURE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR1_SETTING:
                node.log('騒音センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_NOISE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR2_SETTING:
                node.log('騒音センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_NOISE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_ETVOC_SENSOR1_SETTING:
                node.log('eTVOCセンサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_ETVOC)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_ETVOC_SENSOR2_SETTING:
                node.log('eTVOCセンサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_ETVOC)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_ECO2_SENSOR1_SETTING:
                node.log('eCO2センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_ECO2)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_ECO2_SENSOR2_SETTING:
                node.log('eCO2センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_ECO2)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR1_SETTING:
                node.log('不快指数センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_DISCOMFORT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR2_SETTING:
                node.log('不快指数センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_DISCOMFORT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR1_SETTING:
                node.log('熱中症危険度センサー1の設定を行います');
                if(writeEvent1(node, port, USB_EVENT_TARGET_HEAT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR1_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR2_SETTING:
                node.log('熱中症危険度センサー2の設定を行います');
                if(writeEvent2(node, port, USB_EVENT_TARGET_HEAT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR2_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LED_SETTING_EVENT_SETTING:
                node.log('LED setting [event state]の設定を行います');
                if(writeLEDEventState(node, port)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_EVENT_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LED_SETTING_NORMAL_SETTING:
                node.log('LED setting [normal state]の設定を行います');
                if(writeLEDNormalState(node, port)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_NORMAL_SETTING_REQ;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_SETTING_DONE;
                };
                break;


            // センサー設定取得リクエスト中のサブステータス
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_TEMP)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_TEMP)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_HUMIDITY)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_HUMIDITY)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_LIGHT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_LIGHT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_PRESSURE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_PRESSURE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_NOISE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_NOISE)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING;
                };
                break;    
            case INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_ETVOC)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_ETVOC)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR1_GETTING;
                };
                break;  
            case INIT_SUB_STATUS_ECO2_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_ECO2)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_ECO2_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_ECO2)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_DISCOMFORT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_DISCOMFORT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING_REQ:
                if (isNeed2Update1(node, res, USB_EVENT_TARGET_HEAT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR1_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING;
                };
                break;
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING_REQ:
                if (isNeed2Update2(node, res, USB_EVENT_TARGET_HEAT)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR2_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING_REQ:
                if (isNeed2UpdateLEDSettingEvent(node, res)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_EVENT_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING;
                };
                break;
            case INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING_REQ:
                if (isNeed2UpdateLEDSettingNormal(node, res)) {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_NORMAL_SETTING;
                } else {
                    node.nodeStatus.initSubStatus = INIT_SUB_STATUS_SETTING_DONE;
                };
                break;

            // センサー設定リクエスト中のサブステータス
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR1_SETTING_REQ:
                node.log('温度センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_TEMPERATURE_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_TEMPERATURE_SENSOR2_SETTING_REQ:
                node.log('温度センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR1_SETTING_REQ:
                node.log('湿度センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HUMIDITY_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_HUMIDITY_SENSOR2_SETTING_REQ:
                node.log('湿度センサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR1_SETTING_REQ:
                node.log('照度センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LIGHT_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_LIGHT_SENSOR2_SETTING_REQ:
                node.log('照度センサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR1_SETTING_REQ:
                node.log('気圧センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_PRESSURE_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_PRESSURE_SENSOR2_SETTING_REQ:
                node.log('気圧センサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR1_SETTING_REQ:
                node.log('騒音センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_NOISE_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_NOISE_SENSOR2_SETTING_REQ:
                node.log('騒音センサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_ETVOC_SENSOR1_SETTING_REQ:
                node.log('eTVOCセンサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ETVOC_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_ETVOC_SENSOR2_SETTING_REQ:
                node.log('eTVOCセンサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_ECO2_SENSOR1_SETTING_REQ:
                node.log('eCO2センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_ECO2_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_ECO2_SENSOR2_SETTING_REQ:
                node.log('eCO2センサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR1_SETTING_REQ:
                node.log('不快指数センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_DISCOMFORT_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_DISCOMFORT_SENSOR2_SETTING_REQ:
                node.log('不快指数センサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR1_GETTING;
                break;
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR1_SETTING_REQ:
                node.log('熱中症危険度センサー1の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_HEATSTROKE_SENSOR2_GETTING;
                break;
            case INIT_SUB_STATUS_HEATSTROKE_SENSOR2_SETTING_REQ:
                node.log('熱中症危険度センサー2の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_EVENT_GETTING;
                break;
            case INIT_SUB_STATUS_LED_SETTING_EVENT_SETTING_REQ:
                node.log('LED setting [event state]の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_LED_SETTING_NORMAL_GETTING;
                break;
            case INIT_SUB_STATUS_LED_SETTING_NORMAL_SETTING_REQ:
                node.log('LED setting [normal state]の設定完了');
                node.nodeStatus.initSubStatus = INIT_SUB_STATUS_SETTING_DONE;
                break;       
            default:
        }
    }

    /**
     * イベントパターン1の設定処理
     * 
     * @param {*} node 
     * @param {*} port
     * @param {*} event_target ターゲットとなるセンサーを指定
     */
    let writeEvent1 = (node, port, event_target) => {
        let event_req;
        // プロパティ1をチェック
        if (parseInt(node.event_target, 10) === event_target) {
            node.log('プロパティ1で設定を行います');
            event_req = setEvent1(node.event_target, node.event_type, node.event_upper, node.event_lower);
        // プロパティ2をチェック
        } else if (parseInt(node.event2_target, 10) === event_target) {
            node.log('プロパティ2で設定を行います');
            event_req = setEvent1(node.event2_target, node.event2_type, node.event2_upper, node.event2_lower);
        // プロパティ3をチェック
        } else if (parseInt(node.event3_target, 10) === event_target) {
            node.log('プロパティ3で設定を行います');
            event_req = setEvent1(node.event3_target, node.event3_type, node.event3_upper, node.event3_lower);
        // 設定削除の場合
        } else {
            node.log('不要な設定を削除します');
            event_req = setEvent1(event_target, 0, 0, 0);
        }
        if (!writePort(node, port, event_req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
            return false;
        };
        return true;
    }

    /**
     * イベントパターン2の設定処理
     * 
     * @param {*} node 
     * @param {*} port
     * @param {*} event_target ターゲットとなるセンサーを指定
     */
    let writeEvent2 = (node, port, event_target) => {
        let event_req;
        // プロパティ1をチェック
        if ((parseInt(node.event_target, 10) === event_target) && 
            (parseInt(node.event_type, 16) >= USB_EVENT_TYPE_AVERAGE_UPPER)) {
            node.log('プロパティ1で設定を行います');
            event_req = setEvent2(node.event_target, node.event_type, node.event_upper, node.event_lower, node.event_count);
        // プロパティ2をチェック
        } else if ((parseInt(node.event2_target, 10) === event_target) && 
                   (parseInt(node.event2_type, 16) >= USB_EVENT_TYPE_AVERAGE_UPPER)) {
            node.log('プロパティ2で設定を行います');
            event_req = setEvent2(node.event2_target, node.event2_type, node.event2_upper, node.event2_lower, node.event2_count);
        // プロパティ3をチェック
        } else if ((parseInt(node.event3_target, 10) === event_target) && 
                    (parseInt(node.event3_type, 16) >= USB_EVENT_TYPE_AVERAGE_UPPER)) {
            node.log('プロパティ3で設定を行います');
            event_req = setEvent2(node.event3_target, node.event3_type, node.event3_upper, node.event3_lower, node.event3_count);
        } else {
            node.log('設定不要です');
            return false;
        }
        if (!writePort(node, port, event_req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
            return false;
        };
        return true;
    }

    /**
     * LED setting [event state]の設定処理
     * 
     * @param {*} node 
     * @param {*} port
     */
    let writeLEDEventState = (node, port) => {
        const event_req = setLEDSetting(USB_ADDR_LED_SETTING_EVENT, node.led_event_rule, node.led_event_red, node.led_event_green, node.led_event_blue);
        if (!writePort(node, port, event_req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
            return false;
        };
        return true;
    }

    /**
     * LED setting [nomal state]の設定処理
     * 
     * @param {*} node 
     * @param {*} port
     */
    let writeLEDNormalState = (node, port) => {
        const event_req = setLEDSetting(USB_ADDR_LED_SETTING_NORMAL, node.led_normal_rule, node.led_normal_red, node.led_normal_green, node.led_normal_blue);
        if (!writePort(node, port, event_req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
            return false;
        };
        return true;
    }
    
    /**
     * イベントパターン1の設定取得処理
     * 
     * @param {*} node 
     * @param {*} port
     * @param {*} event_target ターゲットとなるセンサー
     */
    let readEvent1 = (node, port, event_target) => {
        let addr = 0x0000; 
        switch(parseInt(event_target, 10)) {
            case USB_EVENT_TARGET_TEMP:
                addr = USB_ADDR_EVENT_TEMP1;
                break;
            case USB_EVENT_TARGET_HUMIDITY:
                addr = USB_ADDR_EVENT_HUMIDITY1;
                break;
            case USB_EVENT_TARGET_LIGHT:
                addr = USB_ADDR_EVENT_LIGHT1;
                break;
            case USB_EVENT_TARGET_PRESSURE:
                addr = USB_ADDR_EVENT_PRESSURE1;
                break;
            case USB_EVENT_TARGET_NOISE:
                addr = USB_ADDR_EVENT_NOISE1;
                break;
            case USB_EVENT_TARGET_ETVOC:
                addr = USB_ADDR_EVENT_ETVOC1;
                break;
            case USB_EVENT_TARGET_ECO2:
                addr = USB_ADDR_EVENT_ECO21;
                break;
            case USB_EVENT_TARGET_DISCOMFORT:
                addr = USB_ADDR_EVENT_DISCOMFORT1;
                break;
            case USB_EVENT_TARGET_HEAT:
                addr = USB_ADDR_EVENT_HEATSTROKE1;
                break;
            default:
        } 
        const req = setFrame(setPayload(USB_CMD_READ, addr, null), 3);
        if (!writePort(node, port, req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
        };
    }

    /**
     * イベントパターン2の設定取得処理
     * 
     * @param {*} node 
     * @param {*} port
     * @param {*} event_target ターゲットとなるセンサー
     */
    let readEvent2 = (node, port, event_target) => {
        let addr = 0x0000; 
        switch(parseInt(event_target, 10)) {
            case USB_EVENT_TARGET_TEMP:
                addr = USB_ADDR_EVENT_TEMP2;
                break;
            case USB_EVENT_TARGET_HUMIDITY:
                addr = USB_ADDR_EVENT_HUMIDITY2;
                break;
            case USB_EVENT_TARGET_LIGHT:
                addr = USB_ADDR_EVENT_LIGHT2;
                break;
            case USB_EVENT_TARGET_PRESSURE:
                addr = USB_ADDR_EVENT_PRESSURE2;
                break;
            case USB_EVENT_TARGET_NOISE:
                addr = USB_ADDR_EVENT_NOISE2;
                break;
            case USB_EVENT_TARGET_ETVOC:
                addr = USB_ADDR_EVENT_ETVOC2;
                break;
            case USB_EVENT_TARGET_ECO2:
                addr = USB_ADDR_EVENT_ECO22;
                break;
            case USB_EVENT_TARGET_DISCOMFORT:
                addr = USB_ADDR_EVENT_DISCOMFORT2;
                break;
            case USB_EVENT_TARGET_HEAT:
                addr = USB_ADDR_EVENT_HEATSTROKE2;
                break;
            default:
        } 
        const req = setFrame(setPayload(USB_CMD_READ, addr, null), 3);
        if (!writePort(node, port, req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
        };
    }

    /**
     * LEDSettingEventの設定取得処理
     * 
     * @param {*} node 
     * @param {*} port
     */
    let readLEDSettingEvent = (node, port) => {
        const req = setFrame(setPayload(USB_CMD_READ, USB_ADDR_LED_SETTING_EVENT, null), 3);
        if (!writePort(node, port, req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
        };
    }

    /**
     * LEDSettingNormalの設定取得処理
     * 
     * @param {*} node 
     * @param {*} port
     */
    let readLEDSettingNormal = (node, port) => {
        const req = setFrame(setPayload(USB_CMD_READ, USB_ADDR_LED_SETTING_NORMAL, null), 3);
        if (!writePort(node, port, req)) {
            node.nodeStatus.mainStatus = NODE_STATUS_FAILED_TO_OPEN_PORT;
        };
    }

    /**
     * イベントパターン1の設定の必要性確認
     * 
     * @param {*} node 
     * @param {*} res
     * @param {*} event_target ターゲットとなるセンサーを指定
     */
    let isNeed2Update1 = (node, res, event_target) => {
        // センサーにイベントが設定されていない場合
        if (res[7] === 0x00 && res[8] === 0x00) { 
            if(isTarget(node, event_target)) {
                // 設定必要(上書き)
                 return true;
            } else {
                // 設定不要
                return false;
            }
        }

        // プロパティのターゲットに指定されていない場合
        if(!isTarget(node, event_target)) {
            // 設定必要(削除)
            return true;
        }

        // プロパティのイベントは一致していない場合
        if(!isEventMatched(node, res, event_target)) {
            // 設定必要(上書き)
            return true;
        };

        // プロパティの閾値が一致していない場合
        if(!isThresholdMatched1(node, res, event_target)) {
            // 設定必要(上書き)
            return true;
        }

        // 設定不要
        return false;
    }

    /**
     * イベントパターン2の設定の必要性確認
     * 
     * @param {*} node 
     * @param {*} res
     * @param {*} event_target ターゲットとなるセンサーを指定
     */
    let isNeed2Update2 = (node, res, event_target) => {
        // プロパティのターゲットに指定されていない場合
        if(!isTarget(node, event_target)) {
            // 設定不要
            return false;
        }

        // プロパティの閾値が一致していない場合
        if(!isThresholdMatched2(node, res, event_target)) {
            // 設定必要(上書き)
            return true;
        }

        // 設定不要
        return false;
    }

    /**
     * LED setting eventの設定の必要性確認
     * 
     * @param {*} node 
     * @param {*} res
     */
    let isNeed2UpdateLEDSettingEvent = (node, res) => {
        const currentDisplayRule = res[7] + res[8] * 256;
        const currentLEDRed = res[9];
        const currentLEDGreen = res[10];
        const currentLEDBlue = res[11];
        if ((currentDisplayRule == parseInt(node.led_event_rule, 16)) &&
            (currentLEDRed == parseInt(node.led_event_red, 10)) &&
            (currentLEDGreen == parseInt(node.led_event_green, 10)) &&
            (currentLEDBlue == parseInt(node.led_event_blue, 10))) {
            return false;
        };      
        return true;
    }

    /**
     * LED setting normalの設定の必要性確認
     * 
     * @param {*} node 
     * @param {*} res
     */
    let isNeed2UpdateLEDSettingNormal = (node, res) => {
        const currentDisplayRule = res[7] + res[8] * 256;
        const currentLEDRed = res[9];
        const currentLEDGreen = res[10];
        const currentLEDBlue = res[11];
        if ((currentDisplayRule == parseInt(node.led_normal_rule, 16)) &&
            (currentLEDRed == parseInt(node.led_normal_red, 10)) &&
            (currentLEDGreen == parseInt(node.led_normal_green, 10)) &&
            (currentLEDBlue == parseInt(node.led_normal_blue, 10))) {
            return false;
        };      
        return true;
    }    

    let isTarget = (node, event_target) => {
        if ((parseInt(node.event_target, 10) === event_target) ||
            (parseInt(node.event2_target, 10) === event_target) || 
            (parseInt(node.event3_target, 10) === event_target)) {
            return true;
        }
        return false;
    }

    let isEventMatched = (node, res, event_target) => {
        const currentEventType = res[7] + res[8] * 256;
        const propertyEventType1 = parseInt(node.event_type, 16);
        const propertyEventType2 = parseInt(node.event2_type, 16);
        const propertyEventType3 = parseInt(node.event3_type, 16);

        // 対象がプロパティ1の場合
        if (parseInt(node.event_target, 10) === event_target) {
            return (propertyEventType1 === currentEventType);
        // 対象がプロパティ2の場合
        } else if (parseInt(node.event2_target, 10) === event_target) {
            return (propertyEventType2 === currentEventType);
        // 対象がプロパティ3の場合
        } else if (parseInt(node.event3_target, 10) === event_target) {
            return (propertyEventType3 === currentEventType);
        }
        return false;
    }

    let isThresholdMatched1 = (node, res, event_target) => {
        // 対象がプロパティ1の場合
        if (parseInt(node.event_target, 10) === event_target) {
            if (parseInt(node.event_type, 16) < USB_EVENT_TYPE_AVERAGE_UPPER){
                return compareThreshold(node.event_type, node.event_upper, node.event_lower, res);
            } else {
                return true;
            }
        // 対象がプロパティ2の場合
        } else if (parseInt(node.event2_target, 10) === event_target) {
            if (parseInt(node.event2_type, 16) < USB_EVENT_TYPE_AVERAGE_UPPER){
                return compareThreshold(node.event2_type, node.event2_upper, node.event2_lower, res);
            } else {
                return true;
            }
        // 対象がプロパティ3の場合
        } else if (parseInt(node.event3_target, 10) === event_target) {
            if (parseInt(node.event3_type, 16) < USB_EVENT_TYPE_AVERAGE_UPPER){
                return compareThreshold(node.event3_type, node.event3_upper, node.event3_lower, res);
            } else {
                return true;
            }
        }
        return false;
    }

    let isThresholdMatched2 = (node, res, event_target) => {
        // 対象がプロパティ1の場合
        if (parseInt(node.event_target, 10) === event_target) {
            if (parseInt(node.event_type, 16) >= USB_EVENT_TYPE_AVERAGE_UPPER){
                return compareThreshold2(node.event_type, node.event_upper, node.event_lower, node.event_count, res);
            } else {
                return true;
            }
        // 対象がプロパティ2の場合
        } else if (parseInt(node.event2_target, 10) === event_target) {
            if (parseInt(node.event2_type, 16) >= USB_EVENT_TYPE_AVERAGE_UPPER){
                return compareThreshold2(node.event2_type, node.event2_upper, node.event2_lower, node.event2_count, res);
            } else {
                return true;
            }
        // 対象がプロパティ3の場合
        } else if (parseInt(node.event3_target, 10) === event_target) {
            if (parseInt(node.event3_type, 16) >= USB_EVENT_TYPE_AVERAGE_UPPER){
                return compareThreshold2(node.event3_type, node.event3_upper, node.event3_lower, node.event3_count, res);
            } else {
                return true;
            }
        }
        return false;
    }

    let compareThreshold = (event_type, event_upper, event_lower, res) => {
        switch(parseInt(event_type, 16)) {
            case USB_EVENT_TYPE_SIMPLE_UPPER:
                if ((res[9] === event_upper % 256) &&
                    (res[10] === event_upper >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_SIMPLE_LOWER:
                if ((res[13] === event_lower % 256) &&
                    (res[14] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_SIMPLE_BOTH:
                if ((res[9] === event_upper % 256) &&
                    (res[10] === event_upper >> 8) &&
                    (res[13] === event_lower % 256) &&
                    (res[14] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_CHANGE_UPPER:
                if ((res[17] === event_upper % 256) &&
                    (res[18] === event_upper >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_CHANGE_LOWER:
                if ((res[21] === event_lower % 256) &&
                    (res[22] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_CHANGE_BOTH:
                if ((res[17] === event_upper % 256) &&
                    (res[18] === event_upper >> 8) &&
                    (res[21] === event_lower % 256) &&
                    (res[22] === event_lower >> 8)) {
                    return true;
                }
                break;
            default:
                break;
        }
        return false;
    }

    let compareThreshold2 = (event_type, event_upper, event_lower, event_count, res) => {

        if ((event_count >= 0x01) && (event_count <= 0x08)) {
            if ((res[23] != event_count) || (res[24] != event_count) && (res[25] != event_count) && (res[26] != event_count)) {
                return false;
            }
        }

        switch(parseInt(event_type, 16)) {
            case USB_EVENT_TYPE_AVERAGE_UPPER:
                if ((res[7] === event_upper % 256) &&
                    (res[8] === event_upper >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_AVERAGE_LOWER:
                if ((res[9] === event_lower % 256) &&
                    (res[10] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_AVERAGE_BOTH:
                if ((res[7] === event_upper % 256) &&
                    (res[8] === event_upper >> 8) &&
                    (res[9] === event_lower % 256) &&
                    (res[10] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_PEEK_UPPER:
                if ((res[11] === event_upper % 256) &&
                    (res[12] === event_upper >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_PEEK_LOWER:
                if ((res[13] === event_lower % 256) &&
                    (res[14] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_PEEK_BOTH:
                if ((res[11] === event_upper % 256) &&
                    (res[12] === event_upper >> 8) &&
                    (res[13] === event_lower % 256) &&
                    (res[14] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_UPPER:
                if ((res[15] === event_upper % 256) &&
                    (res[16] === event_upper >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_LOWER:
                if ((res[17] === event_lower % 256) &&
                    (res[18] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_INTERVAL_DIFF_BOTH:
                if ((res[15] === event_upper % 256) &&
                    (res[16] === event_upper >> 8) &&
                    (res[17] === event_lower % 256) &&
                    (res[18] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_BASE_DIFF_UPPER:
                if ((res[19] === event_upper % 256) &&
                    (res[20] === event_upper >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_BASE_DIFF_LOWER:
                if ((res[21] === event_lower % 256) &&
                    (res[22] === event_lower >> 8)) {
                    return true;
                }
                break;
            case USB_EVENT_TYPE_BASE_DIFF_BOTH:
                if ((res[19] === event_upper % 256) &&
                    (res[20] === event_upper >> 8) &&
                    (res[21] === event_lower % 256) &&
                    (res[22] === event_lower >> 8)) {
                    return true;
                }
                break;
            default:
                node.log("不正なステータス");
                break;
        }
        return false;
    }

    /**
     * Portへの書き込み処理
     * 
     * @param {*} node 
     * @param {*} port
     * @param {*} request
     */
    let writePort = (node, port, request) => {
        port.write(request, function(err) {
            if (err) {
                sendErrorMessage(node, `Failed to write: ${err.message}`, `Failed to write: ${err.message}`);
                node.error('Error on write(event): ', err.message);
                return false;
            }
        });
        return true;
    }

    RED.nodes.registerType("2jcie-bu",Omron2jcieBu);
}
