# @uhuru/node-red-contrib-omron-2jcie-bu

## Installation

To install the stable version use the Menu - Manage palette - Install option and search for node-red-contrib-omron-2jcie-bu, or run the following command in your Node-RED user directory, typically ~/.node-red

```
cd ~/.node-red
npm install @uhuru/node-red-contrib-omron-2jcie-bu
```

## Usage

### Input

#### msg object

For this node, the input msg object is just a trigger.

#### sensor data

This node reads sensor data from a local serial port.
A user must set the serial port name (e.q. '/dev/ttyUSB0' ).

### Output

This node outputs msg objects including sensor data and event flag.
The format of the message is below.

#### Output 1 [sensor data]

```
msg: {
    payload: {
        temperature: 27.43,
        relativeHumidity: 61.18,
        ambientLight: 948,
        barometricPressure: 1005.849,
        soundNoise: 47.09,
        etvoc: 50,
        eco2: 733,
        discomfortIndex: 76.38,
        heatStroke: 24.9
    }
}
```

#### Output 2 [event flag]

```
msg: {
    payload: {
        temperatureEvent: 1,
        relativeHumidityEvent: 0,
        ambientLightEvent: 4,
        barometricPressureEvent: 256,
        soundNoiseEvent: 0,
        etvocEvent: 0,
        eco2Event: 0,
        discomfortIndexEvent: 0,
        heatStrokeEvent: 0,
    }
}
```

## Copyright and license

Copyright Uhuru Coproration under the Apache 2.0 license.